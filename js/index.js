"use strict";

console.log("THEORY");

console.log("1. Дозволяє нам розширювати можливості за ланцюжком, наприклад, клас або метод Машини має опис типу машини (легкова, грузова, військова і тд), клас Електричні має опис робити двигуна (на електроенергії), а клас Nissan описує конкретні електричні машини певної марки, при цьому клас Nissan має можливості верхніх класів але має відмінності і свої унікальні функції в порівняння з однорівневими класами");
console.log("2. Щоб зробити наслідування від обʼєкту вище, наприклад, є клас Тварин, і super дозволяє новоствореному класу Хижаки взяти від нього основні параметри, а потім додати свої");


console.log("PRACTICE");

/* Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). 
Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
Створіть гетери та сеттери для цих властивостей.
Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль. */

let workers = [];

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    };

    get workerName() {
        return this._name;
    };

    set workerName(value) {
        this._name = value;
    };

    get workerAge() {
        return this._age;
    };

    set workerAge(value) {
        if (value < 18) {
            console.log("it's a pupil")
        }
        this._age = value;
    };

    get workerSalary() {
        return this._salary;
    };

    // set workerSalary(value) {
    //     this._salary = value;
    // };

    
};

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
        // this.salary = salary * 3;
    }

    get workerSalary() {
        
        return super.workerSalary * 3;
    }

    get workerName() {
        return super.workerName + "Kitty";
    };

    get workerAge() {
        return super.workerAge;
    };


    // set workerSalary(value) {
    //     super.get();
    //     this._salary = value * 3;
    // };


}

const bob = new Programmer ("Bob", 25, 3000, "Python");
const bob1 = new Programmer ("Robert", 16, 3000, "JS");
const bob2 = new Programmer ("Cleo", 32, 1000, "Java");
console.log(bob);
console.log(bob.workerSalary, bob.workerAge);
console.log(bob1.workerSalary, bob1.workerAge);
console.log(bob2.workerSalary, bob2.workerAge);




console.log(workers);

// let user = {
//     name: "John",
//     surname: "Smith",
  
//     get fullName() {
//       return `${this.name} ${this.surname}`;
//     },
  
//     set fullName(value) {
//       [this.name, this.surname] = value.split(" ");
//     }
//   };



/* const getRandomChance = () => Math.random() < 0.5;

let animals = [];

class Animal {
    constructor(name) {
        this.name = name;
    }

    x = 0;
    div = null;

    createElements() {

        this.div = document.createElement("div")
        this.div.classList.add("animal");
        this.div.innerHTML = `<span>${this.name}</span>`;

        const button = document.createElement("button");
        button.innerText = "Move";
        button.addEventListener("click", this.move.bind(this));
        this.div.append(button);

    }
    
    render(selector = '.container') {
        this.createElements();
        const container = document.querySelector(selector);
        container.append(this.div);
        animals.push(this.div);
    }

    move() {
        this.x += 20;
        this.div.style.transform = `translateX(${this.x}px)`;
    }
}

class Dog extends Animal {
    createElements() {
        super.createElements()
        this.div.classList.add('dog');
    }
    say() {
        this.div.classList.add('dog-say');
        setTimeout(() => {
            this.div.classList.remove('dog-say')
        }, 200)
    }
    move() {
        super.move()
        if(getRandomChance()) {
            super.move()
            this.say()
        }
    }
} */

